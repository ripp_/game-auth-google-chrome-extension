var status="none";
var response="";
var onlineList=[];
var lastStatus = "loggedOut";

var checkInProgress = false;

/*
var ALT_MSG = {
	ERROR:"An unchecked error has occoured with this extension.",
	loggedOut:"",
	loggedIn:"You are now logged into the SUCS Game Server system"
}
*/

function wrapHandler(callback){
	return (function(a){crh(a,callback)});
}

function popupCallback(){
	if (lastStatus != status){
		chrome.notifications.create("",{
			type:"basic",
			iconUrl:"icon.png",
			title:chrome.i18n.getMessage("ALERT_"+status),
			message:chrome.i18n.getMessage("ALERT_MESSAGE_"+status)
		},function(){})
	}
}

function crh(a,sendResponse){
	var wrapped = $("<div>"+a+"</div>"),
		hasForm = wrapped.find("form").length>0,
		alert = $.trim(wrapped.find(".alert").text()),
		user = $.trim(wrapped.find("#username").text()),
		notice = $.trim(wrapped.find("#username").next().text());
	onlineList = wrapped.find(".online-users li").map(function(){return $(this).text()}).toArray()

	if (!user){
		//Not logged in
		if (alert != ""){
			status = "badLogin";
			notice = alert;
		} else {
			status = "loggedOut";
			notice = ""
		}
	} else {
		//Logged in
		if (/banned/.test(notice)){
			status="banned";
		} else if (/only available to SUCS members/.test(notice)) {
			status="nonSucs";
		} else if (/^You are now logged/.test(notice)) {
			status="loggedIn";
		} else {
			//TODO, we need more checkes here
			status="ERROR";
		}
	}
	response = user+" "+chrome.i18n.getMessage("STATUS_"+status);

	if (status == "loggedIn"){
		chrome.browserAction.setIcon({path:"icon.png"})
	} else {
		chrome.browserAction.setIcon({path:"greyIcon.png"})
	}

	/*DEBUG
	console.group("Response");
		console.log(status);
		console.log(response);
		console.log(onlineList);
	console.groupEnd();
	//*/

	checkInProgress = false;
	if(sendResponse){
		sendResponse({status:status,response:response,onlineList:onlineList})
	}
	if (onlineList.length){
		chrome.browserAction.setBadgeText({text:onlineList.length+""});
	} else {
		chrome.browserAction.setBadgeText({text:""});
	}
}

function userConnect(username,password,sendResponse){
	if (checkInProgress) return;
	checkInProgress = true;
	$.post("https://games.sucs.org/",{username:username,password:password},wrapHandler(sendResponse)).always(function(){
		lastStatus = status;
		if(lastStatus == "badLogin") lastStatus = "logout";
		checkInProgress = false;
	});
}

function check(sendResponse){
	if (checkInProgress) return;
	checkInProgress = true;
	$.get("https://games.sucs.org/",wrapHandler(sendResponse)).always(function() {
		lastStatus = status;
		if(lastStatus == "badLogin") lastStatus = "logout";
		checkInProgress = false;
	});
}

function logout(){
	if (checkInProgress) return;
	checkInProgress = true;
	$.post("https://games.sucs.org/",{username:"logout",password:""}).always(function() {
		checkInProgress = false;
	});
	status="loggedOut";
	response=ALT_MSG[status];
	chrome.browserAction.setIcon({path:"greyIcon.png"})
}

function startUp(){
	chrome.storage.local.get(["username","password"],function(items){
		if (items.username && items.password){
			//console.log("logging in with username",items.username) //DEBUG
			userConnect(items.username,items.password,popupCallback)
		} else {
			//console.log("Doing initial check") //DEBUG
			check(popupCallback)
		}
	})
}

//Create Listeners
chrome.runtime.onMessage.addListener(
	function(request, sender, sendResponse) {
		if(request.kind == "open"){
			check(sendResponse)
			return true;
		} else if (request.kind == "logout"){
			logout();
			chrome.storage.local.remove(["username","password"])
			checkInProgress = false;
			sendResponse({status:status,response:response,onlineList:onlineList})
		} else if (request.kind == "login"){
			if (request.store){
				chrome.storage.local.set({username:request.username,password:request.password})
			} else {
				chrome.storage.local.remove(["username","password"]);
			}
			userConnect(request.username,request.password,sendResponse)
			return true;
		}
	}
);

chrome.alarms.create("heartbeat",{delayInMinutes:1,periodInMinutes:1})

chrome.alarms.onAlarm.addListener(function(alarm) {
	if (alarm.name == "heartbeat"){
		check(popupCallback)
	}
});

//Starup with stored values
startUp()