function genericHandler(response){
	$("body").attr("class",response.status);
	$(".responseText").text(response.response);
	$(".onlineList").text(response.onlineList.join(","))
	$("[name=password").val("");
}

//Get info
chrome.runtime.sendMessage({kind:"open"}, genericHandler);

//Login button
$(".login button").click(function(){
	var username = $("[name=username]").val(),
		password = $("[name=password]").val(),
		store = $("[name=store]").prop('checked')
	chrome.runtime.sendMessage({kind:"login",username:username,password:password,store:store}, genericHandler);
})

//Logout button
$(".logout button").click(function(){
	chrome.runtime.sendMessage({kind:"logout"}, genericHandler);
})

$("[data-MSG]").text(function(){
	return chrome.i18n.getMessage($(this).attr("data-MSG"))
});

$("[name=username]").attr("placeholder",chrome.i18n.getMessage("username"))
$("[name=password]").attr("placeholder",chrome.i18n.getMessage("password"))