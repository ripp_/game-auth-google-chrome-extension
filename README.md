Sucs Games Auto Auth
====================

Installation
------------
 1. Download chromeExtension.crx
 2. Open chrome://extensions/ in chrome
 3. Drag the extension from your folder to chrome://extensions/

Features
--------
 * Dosen't require games.sucs.org to be opened in a page.
 * Shows number of players logged in and who.
 * Notifcations if you are logged out during a background check.
 * Optionally remembers username and logs you in on start up.
 * Looks like the login on sucs.org

Current Version
---------------
 * Chrome Extension: v1.0, requires chrome v40

Todo
----
 * Nicer notifcations, espically when logged out.